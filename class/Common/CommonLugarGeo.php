<?php
use \FDSoil\DbFunc as DbFunc;

trait CommonLugarGeo
{

    function lugarGeoEstadoList()
    {
        return DbFunc::exeQryFile( self::_path() ."lugar_geo_estado_list_select.sql", null, false, '', 'common' );         
    }   

    function lugarGeoEstadoListXDes()
    {
        return DbFunc::exeQryFile( self::_path() ."lugar_geo_estado_list_x_des_select.sql", null, false, '', 'common'); 
    }
    
    function lugarGeoMunicipioList()
    {
        return DbFunc::exeQryFile( self::_path() ."lugar_geo_municipio_list_select.sql", $_POST, false, '', 'common'); 
    }

    function lugarGeoMunicipioListXDes($aDes)
    {
        return DbFunc::exeQryFile( self::_path() ."lugar_geo_municipio_list_x_des_select.sql", $aDes, false, '', 'common'); 
    }
    
    function lugarGeoCiudad()
    {
        $row = DbFunc::fetchRow( DbFunc::exeQryFile( self::_path() ."lugar_geo_ciudad_select.sql", $_POST, false, '', 'common') );
        return $row[0].'|'.$row[1];
    }
    
    function lugarGeoParroquiaList()
    {
        return DbFunc::exeQryFile( self::_path() ."lugar_geo_parroquia_list_select.sql", $_POST, false, '', 'common'); 
    }

    function lugarGeoParroquiaListXDes($aDes)
    {
        return DbFunc::exeQryFile( self::_path() ."lugar_geo_parroquia_list_x_des_select.sql", $aDes, false, '', 'common'); 
    }
    
    function lugarGeoPaisList($aIds)
    {
        return DbFunc::exeQryFile( self::_path() ."lugar_geo_pais_list_select.sql", $aIds, false, '', 'common');
    }
    
    function lugarGeoCodTelefonoInternacionalList()
    {
        return DbFunc::exeQryFile( self::_path() ."lugar_geo_cod_telefono_internacional_list_select.sql", null, false, '', 'common');
    }
    
    function lugarGeoCodTelefonoNacionalList()
    {
        return DbFunc::exeQryFile( self::_path() ."lugar_geo_cod_telefono_nacional_list_select.sql", null, false, '', 'common');
    } 

    function lugarGeoCodCelularNacionalList()
    {
        return DbFunc::exeQryFile( self::_path() ."lugar_geo_cod_celular_nacional_list_select.sql", null, false, '', 'common');
    } 
    
    function lugarGeoMonedaList()
    {
        return DbFunc::exeQryFile( self::_path() ."lugar_geo_moneda_list_select.sql", null, false, '', 'common');
    }
    
    function paisGetData()
    {
        return DbFunc::exeQryFile( self::_path() . "lugar_geo_pais_list_all_select.sql", null, false, '', 'common');
    }
    
    function lugarGeoPaisInList($aIds)
    {
        return DbFunc::exeQryFile( self::_path() ."lugar_geo_pais_list_in_select.sql", $aIds, false, '', 'common');
    }
    
    function lugarGeoInmuebleList()
    {
        return DbFunc::exeQryFile( self::_path() ."lugar_geo_inmueble_select.sql", null, false, '', 'common');
    }  
   
}
