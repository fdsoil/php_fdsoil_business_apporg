<?php
use \FDSoil\DbFunc as DbFunc;

trait CommonVehiculo 
{

    function vehiculoTipoList() { return DbFunc::exeQryFile(self::_path()."vehiculo_tipo_list_select.sql", null, false, '', 'common'); }
    
    function vehiculoMarcaList($aId) { return DbFunc::exeQryFile(self::_path()."vehiculo_marca_list_select.sql", $aId, false, '', 'common'); }
    
    function vehiculoModeloList($aId) { return DbFunc::exeQryFile(self::_path()."vehiculo_modelo_list_select.sql", $aId, false, '', 'common'); }

    function plataformaTipoList() { return DbFunc::exeQryFile(self::_path()."plataforma_tipo_list_select.sql", null, false, '', 'common'); }

    function cisternaTipoList() { return DbFunc::exeQryFile(self::_path()."cisterna_tipo_list_select.sql", null, false, '', 'common'); }

//----------------TIPO DE VEHICULO----------------------------------------------

    function InsertTipoVehiculo()
    {
        $sql = DbFunc::fetchRow(DbFunc::exeQryFile(self::_path(). "vehiculo_tipo_save.sql", $_POST, false, '', 'common'));
        return $sql[0];
    }
        
    function SelectTipoVehiculo() { return DbFunc::exeQryFile(self::_path(). "vehiculo_tipo_select.sql", $_POST, false, '', 'common'); }

    function ListarTipoVehiculo() { return DbFunc::exeQryFile(self::_path(). 'vehiculo_tipo_listar.sql', $_POST, false, '', 'common'); }

    //------------------MARCA DE VEHICULO---------------------------------------------------

    function InsertMarcaVehiculo()
    {
        $sql = DbFunc::fetchRow(DbFunc::exeQryFile(self::_path(). "vehiculo_marca_save.sql", $_POST, false, '', 'common'));
        return $sql[0];
    }
        
    
    function SelectMarcaVehiculo() { return DbFunc::exeQryFile(self::_path(). "vehiculo_marca_select.sql", $_POST, false, '', 'common'); }

    function ListarMarcaVehiculo() { return DbFunc::exeQryFile(self::_path(). 'vehiculo_marca_listar.sql', $_POST, false, '', 'common'); }

    //-------------------MODELO DE VEHICULO--------------------------------------
    
    function InsertModeloVehiculo()
    {
        $sql = DbFunc::fetchRow(DbFunc::exeQryFile(self::_path(). "vehiculo_modelo_save.sql", $_POST, false, '', 'common'));
        return $sql[0];
    }

    function SelectModeloVehiculo() { return DbFunc::exeQryFile(self::_path(). "vehiculo_modelo_select.sql", $_POST, false, '', 'common'); }

    function ListarModeloVehiculo() { return DbFunc::exeQryFile(self::_path(). 'vehiculo_modelo_listar.sql', $_POST, false, '', 'common'); }
   
}

