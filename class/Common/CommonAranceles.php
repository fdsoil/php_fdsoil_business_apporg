<?php
use \FDSoil\DbFunc as DbFunc;

trait CommonAranceles 
{    
   
    function arancelesList() {
        if ($_POST === []) 
            $_POST['codprocedencia'] = 0;
        return DbFunc::exeQryFile(self::_path()."aranceles_aranceles_list_select.sql", $_POST, false, '', 'common');
    }
    
    function commonCodAndDescArancel(){
        return DbFunc::fetchRow(
            DbFunc::exeQryFile(self::_path()."aranceles_aranceles_cod_and_desc_arancel_select.sql", $_POST, false, '', 'common')
        );
    }
}

