<?php
use \appOrg\Common as Common;
use \FDSoil\DbFunc as DbFunc;

class SubIndex
{

    public function __construct($method) { self::$method(); }

    private function vehiculoMarcaList() { echo DbFunc::resultToString(Common::vehiculoMarcaList($_POST),'#',"%"); }

    private function vehiculoModeloList() { echo DbFunc::resultToString(Common::vehiculoModeloList($_POST),'#',"%"); }

}
