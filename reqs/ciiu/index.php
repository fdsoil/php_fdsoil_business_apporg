<?php
use \appOrg\Common as Common;
use \FDSoil\DbFunc as DbFunc;

class SubIndex
{

    public function __construct($method) { self::$method(); }

    private function ciiuDivisionList() { echo DbFunc::resultToString(Common::ciiuDivisionList($_POST),'#',"|"); }

    private function ciiuGrupoList() { echo DbFunc::resultToString(Common::ciiuGrupoList($_POST),'#',"|"); }

    private function ciiuClaseList() { echo DbFunc::resultToString(Common::ciiuClaseList($_POST),'#',"|"); }

}

